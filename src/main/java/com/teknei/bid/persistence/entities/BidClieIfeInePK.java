package com.teknei.bid.persistence.entities;

import javax.persistence.Column;
import javax.persistence.Id;
import java.io.Serializable;
import java.util.Objects;

public class BidClieIfeInePK implements Serializable {
    private long idClie;
    private long idIfe;

    @Column(name = "id_clie")
    @Id
    public long getIdClie() {
        return idClie;
    }

    public void setIdClie(long idClie) {
        this.idClie = idClie;
    }

    @Column(name = "id_ife")
    @Id
    public long getIdIfe() {
        return idIfe;
    }

    public void setIdIfe(long idIfe) {
        this.idIfe = idIfe;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        BidClieIfeInePK that = (BidClieIfeInePK) o;
        return idClie == that.idClie &&
                idIfe == that.idIfe;
    }

    @Override
    public int hashCode() {

        return Objects.hash(idClie, idIfe);
    }
}
