package com.teknei.bid.persistence.entities;

import javax.persistence.Column;
import javax.persistence.Id;
import java.io.Serializable;
import java.util.Objects;

public class BidEmprDispPK implements Serializable {
    private long idEmpr;
    private long idDisp;

    @Column(name = "id_empr")
    @Id
    public long getIdEmpr() {
        return idEmpr;
    }

    public void setIdEmpr(long idEmpr) {
        this.idEmpr = idEmpr;
    }

    @Column(name = "id_disp")
    @Id
    public long getIdDisp() {
        return idDisp;
    }

    public void setIdDisp(long idDisp) {
        this.idDisp = idDisp;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        BidEmprDispPK that = (BidEmprDispPK) o;
        return idEmpr == that.idEmpr &&
                idDisp == that.idDisp;
    }

    @Override
    public int hashCode() {

        return Objects.hash(idEmpr, idDisp);
    }
}
