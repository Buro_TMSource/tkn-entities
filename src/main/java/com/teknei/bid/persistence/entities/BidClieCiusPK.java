package com.teknei.bid.persistence.entities;

import javax.persistence.Column;
import javax.persistence.Id;
import java.io.Serializable;
import java.util.Objects;

public class BidClieCiusPK implements Serializable {
    private long idClie;
    private long idCius;

    @Column(name = "id_clie")
    @Id
    public long getIdClie() {
        return idClie;
    }

    public void setIdClie(long idClie) {
        this.idClie = idClie;
    }

    @Column(name = "id_cius")
    @Id
    public long getIdCius() {
        return idCius;
    }

    public void setIdCius(long idCius) {
        this.idCius = idCius;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        BidClieCiusPK that = (BidClieCiusPK) o;
        return idClie == that.idClie &&
                idCius == that.idCius;
    }

    @Override
    public int hashCode() {

        return Objects.hash(idClie, idCius);
    }
}
