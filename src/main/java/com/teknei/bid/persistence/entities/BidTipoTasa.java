package com.teknei.bid.persistence.entities;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Objects;

@Entity
@Table(name = "bid_tipo_tasa", schema = "bid", catalog = "bid")
public class BidTipoTasa {
    private long idTipoTasa;
    private String codTasa;
    private String descTasa;
    private int idEsta;
    private int idTipo;
    private String usrCrea;
    private Timestamp fchCrea;
    private String usrModi;
    private Timestamp fchModi;

    @Id
    @Column(name = "id_tipo_tasa")
    public long getIdTipoTasa() {
        return idTipoTasa;
    }

    public void setIdTipoTasa(long idTipoTasa) {
        this.idTipoTasa = idTipoTasa;
    }

    @Basic
    @Column(name = "cod_tasa")
    public String getCodTasa() {
        return codTasa;
    }

    public void setCodTasa(String codTasa) {
        this.codTasa = codTasa;
    }

    @Basic
    @Column(name = "desc_tasa")
    public String getDescTasa() {
        return descTasa;
    }

    public void setDescTasa(String descTasa) {
        this.descTasa = descTasa;
    }

    @Basic
    @Column(name = "id_esta")
    public int getIdEsta() {
        return idEsta;
    }

    public void setIdEsta(int idEsta) {
        this.idEsta = idEsta;
    }

    @Basic
    @Column(name = "id_tipo")
    public int getIdTipo() {
        return idTipo;
    }

    public void setIdTipo(int idTipo) {
        this.idTipo = idTipo;
    }

    @Basic
    @Column(name = "usr_crea")
    public String getUsrCrea() {
        return usrCrea;
    }

    public void setUsrCrea(String usrCrea) {
        this.usrCrea = usrCrea;
    }

    @Basic
    @Column(name = "fch_crea")
    public Timestamp getFchCrea() {
        return fchCrea;
    }

    public void setFchCrea(Timestamp fchCrea) {
        this.fchCrea = fchCrea;
    }

    @Basic
    @Column(name = "usr_modi")
    public String getUsrModi() {
        return usrModi;
    }

    public void setUsrModi(String usrModi) {
        this.usrModi = usrModi;
    }

    @Basic
    @Column(name = "fch_modi")
    public Timestamp getFchModi() {
        return fchModi;
    }

    public void setFchModi(Timestamp fchModi) {
        this.fchModi = fchModi;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        BidTipoTasa that = (BidTipoTasa) o;
        return idTipoTasa == that.idTipoTasa &&
                idEsta == that.idEsta &&
                idTipo == that.idTipo &&
                Objects.equals(codTasa, that.codTasa) &&
                Objects.equals(descTasa, that.descTasa) &&
                Objects.equals(usrCrea, that.usrCrea) &&
                Objects.equals(fchCrea, that.fchCrea) &&
                Objects.equals(usrModi, that.usrModi) &&
                Objects.equals(fchModi, that.fchModi);
    }

    @Override
    public int hashCode() {

        return Objects.hash(idTipoTasa, codTasa, descTasa, idEsta, idTipo, usrCrea, fchCrea, usrModi, fchModi);
    }
}
