package com.teknei.bid.persistence.entities;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Objects;

@Entity
@Table(name = "bid_tipo_plat", schema = "bid", catalog = "bid")
public class BidTipoPlat {
    private long idTipoPlat;
    private String codTipoPlat;
    private String descTipoPlat;
    private int idEsta;
    private int idTipo;
    private String usrCrea;
    private Timestamp fchCrea;
    private String usrModi;
    private Timestamp fchModi;

    @Id
    @Column(name = "id_tipo_plat")
    public long getIdTipoPlat() {
        return idTipoPlat;
    }

    public void setIdTipoPlat(long idTipoPlat) {
        this.idTipoPlat = idTipoPlat;
    }

    @Basic
    @Column(name = "cod_tipo_plat")
    public String getCodTipoPlat() {
        return codTipoPlat;
    }

    public void setCodTipoPlat(String codTipoPlat) {
        this.codTipoPlat = codTipoPlat;
    }

    @Basic
    @Column(name = "desc_tipo_plat")
    public String getDescTipoPlat() {
        return descTipoPlat;
    }

    public void setDescTipoPlat(String descTipoPlat) {
        this.descTipoPlat = descTipoPlat;
    }

    @Basic
    @Column(name = "id_esta")
    public int getIdEsta() {
        return idEsta;
    }

    public void setIdEsta(int idEsta) {
        this.idEsta = idEsta;
    }

    @Basic
    @Column(name = "id_tipo")
    public int getIdTipo() {
        return idTipo;
    }

    public void setIdTipo(int idTipo) {
        this.idTipo = idTipo;
    }

    @Basic
    @Column(name = "usr_crea")
    public String getUsrCrea() {
        return usrCrea;
    }

    public void setUsrCrea(String usrCrea) {
        this.usrCrea = usrCrea;
    }

    @Basic
    @Column(name = "fch_crea")
    public Timestamp getFchCrea() {
        return fchCrea;
    }

    public void setFchCrea(Timestamp fchCrea) {
        this.fchCrea = fchCrea;
    }

    @Basic
    @Column(name = "usr_modi")
    public String getUsrModi() {
        return usrModi;
    }

    public void setUsrModi(String usrModi) {
        this.usrModi = usrModi;
    }

    @Basic
    @Column(name = "fch_modi")
    public Timestamp getFchModi() {
        return fchModi;
    }

    public void setFchModi(Timestamp fchModi) {
        this.fchModi = fchModi;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        BidTipoPlat that = (BidTipoPlat) o;
        return idTipoPlat == that.idTipoPlat &&
                idEsta == that.idEsta &&
                idTipo == that.idTipo &&
                Objects.equals(codTipoPlat, that.codTipoPlat) &&
                Objects.equals(descTipoPlat, that.descTipoPlat) &&
                Objects.equals(usrCrea, that.usrCrea) &&
                Objects.equals(fchCrea, that.fchCrea) &&
                Objects.equals(usrModi, that.usrModi) &&
                Objects.equals(fchModi, that.fchModi);
    }

    @Override
    public int hashCode() {

        return Objects.hash(idTipoPlat, codTipoPlat, descTipoPlat, idEsta, idTipo, usrCrea, fchCrea, usrModi, fchModi);
    }
}
