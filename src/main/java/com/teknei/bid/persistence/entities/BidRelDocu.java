package com.teknei.bid.persistence.entities;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Objects;

@Entity
@Table(name = "bid_rel_docu", schema = "bid", catalog = "bid")
@IdClass(BidRelDocuPK.class)
public class BidRelDocu {
    private long idDocuPrim;
    private long idDocuSecu;
    private int idEsta;
    private int idTipo;
    private String usrCrea;
    private Timestamp fchCrea;
    private String usrModi;
    private Timestamp fchModi;

    @Id
    @Column(name = "id_docu_prim")
    public long getIdDocuPrim() {
        return idDocuPrim;
    }

    public void setIdDocuPrim(long idDocuPrim) {
        this.idDocuPrim = idDocuPrim;
    }

    @Id
    @Column(name = "id_docu_secu")
    public long getIdDocuSecu() {
        return idDocuSecu;
    }

    public void setIdDocuSecu(long idDocuSecu) {
        this.idDocuSecu = idDocuSecu;
    }

    @Basic
    @Column(name = "id_esta")
    public int getIdEsta() {
        return idEsta;
    }

    public void setIdEsta(int idEsta) {
        this.idEsta = idEsta;
    }

    @Basic
    @Column(name = "id_tipo")
    public int getIdTipo() {
        return idTipo;
    }

    public void setIdTipo(int idTipo) {
        this.idTipo = idTipo;
    }

    @Basic
    @Column(name = "usr_crea")
    public String getUsrCrea() {
        return usrCrea;
    }

    public void setUsrCrea(String usrCrea) {
        this.usrCrea = usrCrea;
    }

    @Basic
    @Column(name = "fch_crea")
    public Timestamp getFchCrea() {
        return fchCrea;
    }

    public void setFchCrea(Timestamp fchCrea) {
        this.fchCrea = fchCrea;
    }

    @Basic
    @Column(name = "usr_modi")
    public String getUsrModi() {
        return usrModi;
    }

    public void setUsrModi(String usrModi) {
        this.usrModi = usrModi;
    }

    @Basic
    @Column(name = "fch_modi")
    public Timestamp getFchModi() {
        return fchModi;
    }

    public void setFchModi(Timestamp fchModi) {
        this.fchModi = fchModi;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        BidRelDocu that = (BidRelDocu) o;
        return idDocuPrim == that.idDocuPrim &&
                idDocuSecu == that.idDocuSecu &&
                idEsta == that.idEsta &&
                idTipo == that.idTipo &&
                Objects.equals(usrCrea, that.usrCrea) &&
                Objects.equals(fchCrea, that.fchCrea) &&
                Objects.equals(usrModi, that.usrModi) &&
                Objects.equals(fchModi, that.fchModi);
    }

    @Override
    public int hashCode() {

        return Objects.hash(idDocuPrim, idDocuSecu, idEsta, idTipo, usrCrea, fchCrea, usrModi, fchModi);
    }
}
