package com.teknei.bid.persistence.entities;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Objects;

@Entity
@Table(name = "bid_clie_dire_nest", schema = "bid", catalog = "bid")
@IdClass(BidClieDireNestPK.class)
public class BidClieDireNest {
    private long idClie;
    private long idDire;
    private String dirNestUno;
    private String dirNestDos;
    private int idTipo;
    private int idEsta;
    private String usrCrea;
    private Timestamp fchCrea;
    private String usrModi;
    private Timestamp fchModi;
    private String usrOpeCrea;
    private String usrOpeModi;

    @Id
    @Column(name = "id_clie")
    public long getIdClie() {
        return idClie;
    }

    public void setIdClie(long idClie) {
        this.idClie = idClie;
    }

    @Id
    @Column(name = "id_dire")
    public long getIdDire() {
        return idDire;
    }

    public void setIdDire(long idDire) {
        this.idDire = idDire;
    }

    @Basic
    @Column(name = "dir_nest_uno")
    public String getDirNestUno() {
        return dirNestUno;
    }

    public void setDirNestUno(String dirNestUno) {
        this.dirNestUno = dirNestUno;
    }

    @Basic
    @Column(name = "dir_nest_dos")
    public String getDirNestDos() {
        return dirNestDos;
    }

    public void setDirNestDos(String dirNestDos) {
        this.dirNestDos = dirNestDos;
    }

    @Basic
    @Column(name = "id_tipo")
    public int getIdTipo() {
        return idTipo;
    }

    public void setIdTipo(int idTipo) {
        this.idTipo = idTipo;
    }

    @Basic
    @Column(name = "id_esta")
    public int getIdEsta() {
        return idEsta;
    }

    public void setIdEsta(int idEsta) {
        this.idEsta = idEsta;
    }

    @Basic
    @Column(name = "usr_crea")
    public String getUsrCrea() {
        return usrCrea;
    }

    public void setUsrCrea(String usrCrea) {
        this.usrCrea = usrCrea;
    }

    @Basic
    @Column(name = "fch_crea")
    public Timestamp getFchCrea() {
        return fchCrea;
    }

    public void setFchCrea(Timestamp fchCrea) {
        this.fchCrea = fchCrea;
    }

    @Basic
    @Column(name = "usr_modi")
    public String getUsrModi() {
        return usrModi;
    }

    public void setUsrModi(String usrModi) {
        this.usrModi = usrModi;
    }

    @Basic
    @Column(name = "fch_modi")
    public Timestamp getFchModi() {
        return fchModi;
    }

    public void setFchModi(Timestamp fchModi) {
        this.fchModi = fchModi;
    }

    @Basic
    @Column(name = "usr_ope_crea")
    public String getUsrOpeCrea() {
        return usrOpeCrea;
    }

    public void setUsrOpeCrea(String usrOpeCrea) {
        this.usrOpeCrea = usrOpeCrea;
    }

    @Basic
    @Column(name = "usr_ope_modi")
    public String getUsrOpeModi() {
        return usrOpeModi;
    }

    public void setUsrOpeModi(String usrOpeModi) {
        this.usrOpeModi = usrOpeModi;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        BidClieDireNest that = (BidClieDireNest) o;
        return idClie == that.idClie &&
                idDire == that.idDire &&
                idTipo == that.idTipo &&
                idEsta == that.idEsta &&
                Objects.equals(dirNestUno, that.dirNestUno) &&
                Objects.equals(dirNestDos, that.dirNestDos) &&
                Objects.equals(usrCrea, that.usrCrea) &&
                Objects.equals(fchCrea, that.fchCrea) &&
                Objects.equals(usrModi, that.usrModi) &&
                Objects.equals(fchModi, that.fchModi) &&
                Objects.equals(usrOpeCrea, that.usrOpeCrea) &&
                Objects.equals(usrOpeModi, that.usrOpeModi);
    }

    @Override
    public int hashCode() {

        return Objects.hash(idClie, idDire, dirNestUno, dirNestDos, idTipo, idEsta, usrCrea, fchCrea, usrModi, fchModi, usrOpeCrea, usrOpeModi);
    }
}
