package com.teknei.bid.persistence.entities;

import javax.persistence.Column;
import javax.persistence.Id;
import java.io.Serializable;
import java.util.Objects;

public class BidEmprUsuaPK implements Serializable {
    private long idEmpr;
    private long idUsua;

    @Column(name = "id_empr")
    @Id
    public long getIdEmpr() {
        return idEmpr;
    }

    public void setIdEmpr(long idEmpr) {
        this.idEmpr = idEmpr;
    }

    @Column(name = "id_usua")
    @Id
    public long getIdUsua() {
        return idUsua;
    }

    public void setIdUsua(long idUsua) {
        this.idUsua = idUsua;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        BidEmprUsuaPK that = (BidEmprUsuaPK) o;
        return idEmpr == that.idEmpr &&
                idUsua == that.idUsua;
    }

    @Override
    public int hashCode() {

        return Objects.hash(idEmpr, idUsua);
    }
}
