package com.teknei.bid.persistence.entities;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Objects;

@Entity
@Table(name = "bid_usua", schema = "bid", catalog = "bid")
public class BidUsua {
    private long idUsua;
    private String usua;
    private String pass;
    private int idEsta;
    private int idTipo;
    private String usrCrea;
    private Timestamp fchCrea;
    private String usrModi;
    private Timestamp fchModi;
    private Long idClie;
    private String usrOpeCrea;
    private String usrOpeModi;

    @Id
    @SequenceGenerator(schema = "bid", name = "BID_USUA_SEQ", sequenceName = "bid.bid_usua_id_usua_seq", allocationSize = 1, catalog = "bid")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "BID_USUA_SEQ")
    @Column(name = "id_usua")
    public long getIdUsua() {
        return idUsua;
    }

    public void setIdUsua(long idUsua) {
        this.idUsua = idUsua;
    }

    @Basic
    @Column(name = "usua")
    public String getUsua() {
        return usua;
    }

    public void setUsua(String usua) {
        this.usua = usua;
    }

    @Basic
    @Column(name = "pass")
    public String getPass() {
        return pass;
    }

    public void setPass(String pass) {
        this.pass = pass;
    }

    @Basic
    @Column(name = "id_esta")
    public int getIdEsta() {
        return idEsta;
    }

    public void setIdEsta(int idEsta) {
        this.idEsta = idEsta;
    }

    @Basic
    @Column(name = "id_tipo")
    public int getIdTipo() {
        return idTipo;
    }

    public void setIdTipo(int idTipo) {
        this.idTipo = idTipo;
    }

    @Basic
    @Column(name = "usr_crea")
    public String getUsrCrea() {
        return usrCrea;
    }

    public void setUsrCrea(String usrCrea) {
        this.usrCrea = usrCrea;
    }

    @Basic
    @Column(name = "fch_crea")
    public Timestamp getFchCrea() {
        return fchCrea;
    }

    public void setFchCrea(Timestamp fchCrea) {
        this.fchCrea = fchCrea;
    }

    @Basic
    @Column(name = "usr_modi")
    public String getUsrModi() {
        return usrModi;
    }

    public void setUsrModi(String usrModi) {
        this.usrModi = usrModi;
    }

    @Basic
    @Column(name = "fch_modi")
    public Timestamp getFchModi() {
        return fchModi;
    }

    public void setFchModi(Timestamp fchModi) {
        this.fchModi = fchModi;
    }

    @Basic
    @Column(name = "id_clie")
    public Long getIdClie() {
        return idClie;
    }

    public void setIdClie(Long idClie) {
        this.idClie = idClie;
    }

    @Basic
    @Column(name = "usr_ope_crea")
    public String getUsrOpeCrea() {
        return usrOpeCrea;
    }

    public void setUsrOpeCrea(String usrOpeCrea) {
        this.usrOpeCrea = usrOpeCrea;
    }

    @Basic
    @Column(name = "usr_ope_modi")
    public String getUsrOpeModi() {
        return usrOpeModi;
    }

    public void setUsrOpeModi(String usrOpeModi) {
        this.usrOpeModi = usrOpeModi;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        BidUsua bidUsua = (BidUsua) o;
        return idUsua == bidUsua.idUsua &&
                idEsta == bidUsua.idEsta &&
                idTipo == bidUsua.idTipo &&
                Objects.equals(usua, bidUsua.usua) &&
                Objects.equals(pass, bidUsua.pass) &&
                Objects.equals(usrCrea, bidUsua.usrCrea) &&
                Objects.equals(fchCrea, bidUsua.fchCrea) &&
                Objects.equals(usrModi, bidUsua.usrModi) &&
                Objects.equals(fchModi, bidUsua.fchModi) &&
                Objects.equals(idClie, bidUsua.idClie) &&
                Objects.equals(usrOpeCrea, bidUsua.usrOpeCrea) &&
                Objects.equals(usrOpeModi, bidUsua.usrOpeModi);
    }

    @Override
    public int hashCode() {

        return Objects.hash(idUsua, usua, pass, idEsta, idTipo, usrCrea, fchCrea, usrModi, fchModi, idClie, usrOpeCrea, usrOpeModi);
    }
}
