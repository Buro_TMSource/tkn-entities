package com.teknei.bid.persistence.entities;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Objects;

@Entity
@Table(name = "bid_regi", schema = "bid", catalog = "bid")
public class BidRegi {
    private long idRegi;
    private long idEmpr;
    private long idClie;
    private long idDire;
    private Long idEmai;
    private Long idIfe;
    private long idUsua;
    private long idDisp;
    private int idEsta;
    private int idTipo;
    private String usrCrea;
    private Timestamp fchCrea;
    private String usrModi;
    private Timestamp fchModi;
    private String usrOpeCrea;
    private String usrOpeModi;

    @Id
    @Column(name = "id_regi")
    public long getIdRegi() {
        return idRegi;
    }

    public void setIdRegi(long idRegi) {
        this.idRegi = idRegi;
    }

    @Basic
    @Column(name = "id_empr")
    public long getIdEmpr() {
        return idEmpr;
    }

    public void setIdEmpr(long idEmpr) {
        this.idEmpr = idEmpr;
    }

    @Basic
    @Column(name = "id_clie")
    public long getIdClie() {
        return idClie;
    }

    public void setIdClie(long idClie) {
        this.idClie = idClie;
    }

    @Basic
    @Column(name = "id_dire")
    public long getIdDire() {
        return idDire;
    }

    public void setIdDire(long idDire) {
        this.idDire = idDire;
    }

    @Basic
    @Column(name = "id_emai")
    public Long getIdEmai() {
        return idEmai;
    }

    public void setIdEmai(Long idEmai) {
        this.idEmai = idEmai;
    }

    @Basic
    @Column(name = "id_ife")
    public Long getIdIfe() {
        return idIfe;
    }

    public void setIdIfe(Long idIfe) {
        this.idIfe = idIfe;
    }

    @Basic
    @Column(name = "id_usua")
    public long getIdUsua() {
        return idUsua;
    }

    public void setIdUsua(long idUsua) {
        this.idUsua = idUsua;
    }

    @Basic
    @Column(name = "id_disp")
    public long getIdDisp() {
        return idDisp;
    }

    public void setIdDisp(long idDisp) {
        this.idDisp = idDisp;
    }

    @Basic
    @Column(name = "id_esta")
    public int getIdEsta() {
        return idEsta;
    }

    public void setIdEsta(int idEsta) {
        this.idEsta = idEsta;
    }

    @Basic
    @Column(name = "id_tipo")
    public int getIdTipo() {
        return idTipo;
    }

    public void setIdTipo(int idTipo) {
        this.idTipo = idTipo;
    }

    @Basic
    @Column(name = "usr_crea")
    public String getUsrCrea() {
        return usrCrea;
    }

    public void setUsrCrea(String usrCrea) {
        this.usrCrea = usrCrea;
    }

    @Basic
    @Column(name = "fch_crea")
    public Timestamp getFchCrea() {
        return fchCrea;
    }

    public void setFchCrea(Timestamp fchCrea) {
        this.fchCrea = fchCrea;
    }

    @Basic
    @Column(name = "usr_modi")
    public String getUsrModi() {
        return usrModi;
    }

    public void setUsrModi(String usrModi) {
        this.usrModi = usrModi;
    }

    @Basic
    @Column(name = "fch_modi")
    public Timestamp getFchModi() {
        return fchModi;
    }

    public void setFchModi(Timestamp fchModi) {
        this.fchModi = fchModi;
    }

    @Basic
    @Column(name = "usr_ope_crea")
    public String getUsrOpeCrea() {
        return usrOpeCrea;
    }

    public void setUsrOpeCrea(String usrOpeCrea) {
        this.usrOpeCrea = usrOpeCrea;
    }

    @Basic
    @Column(name = "usr_ope_modi")
    public String getUsrOpeModi() {
        return usrOpeModi;
    }

    public void setUsrOpeModi(String usrOpeModi) {
        this.usrOpeModi = usrOpeModi;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        BidRegi bidRegi = (BidRegi) o;
        return idRegi == bidRegi.idRegi &&
                idEmpr == bidRegi.idEmpr &&
                idClie == bidRegi.idClie &&
                idDire == bidRegi.idDire &&
                idUsua == bidRegi.idUsua &&
                idDisp == bidRegi.idDisp &&
                idEsta == bidRegi.idEsta &&
                idTipo == bidRegi.idTipo &&
                Objects.equals(idEmai, bidRegi.idEmai) &&
                Objects.equals(idIfe, bidRegi.idIfe) &&
                Objects.equals(usrCrea, bidRegi.usrCrea) &&
                Objects.equals(fchCrea, bidRegi.fchCrea) &&
                Objects.equals(usrModi, bidRegi.usrModi) &&
                Objects.equals(fchModi, bidRegi.fchModi) &&
                Objects.equals(usrOpeCrea, bidRegi.usrOpeCrea) &&
                Objects.equals(usrOpeModi, bidRegi.usrOpeModi);
    }

    @Override
    public int hashCode() {

        return Objects.hash(idRegi, idEmpr, idClie, idDire, idEmai, idIfe, idUsua, idDisp, idEsta, idTipo, usrCrea, fchCrea, usrModi, fchModi, usrOpeCrea, usrOpeModi);
    }
}
