package com.teknei.bid.persistence.entities;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Objects;

@Entity
@Table(name = "bid_esta_proc", schema = "bid", catalog = "bid")
public class BidEstaProc {
    private long idEstaProc;
    private String codEstaProc;
    private String descEstaProc;
    private int idEsta;
    private int idTipo;
    private String usrCrea;
    private Timestamp fchCrea;
    private String usrModi;
    private Timestamp fchModi;

    @Id
    @Column(name = "id_esta_proc")
    public long getIdEstaProc() {
        return idEstaProc;
    }

    public void setIdEstaProc(long idEstaProc) {
        this.idEstaProc = idEstaProc;
    }

    @Basic
    @Column(name = "cod_esta_proc")
    public String getCodEstaProc() {
        return codEstaProc;
    }

    public void setCodEstaProc(String codEstaProc) {
        this.codEstaProc = codEstaProc;
    }

    @Basic
    @Column(name = "desc_esta_proc")
    public String getDescEstaProc() {
        return descEstaProc;
    }

    public void setDescEstaProc(String descEstaProc) {
        this.descEstaProc = descEstaProc;
    }

    @Basic
    @Column(name = "id_esta")
    public int getIdEsta() {
        return idEsta;
    }

    public void setIdEsta(int idEsta) {
        this.idEsta = idEsta;
    }

    @Basic
    @Column(name = "id_tipo")
    public int getIdTipo() {
        return idTipo;
    }

    public void setIdTipo(int idTipo) {
        this.idTipo = idTipo;
    }

    @Basic
    @Column(name = "usr_crea")
    public String getUsrCrea() {
        return usrCrea;
    }

    public void setUsrCrea(String usrCrea) {
        this.usrCrea = usrCrea;
    }

    @Basic
    @Column(name = "fch_crea")
    public Timestamp getFchCrea() {
        return fchCrea;
    }

    public void setFchCrea(Timestamp fchCrea) {
        this.fchCrea = fchCrea;
    }

    @Basic
    @Column(name = "usr_modi")
    public String getUsrModi() {
        return usrModi;
    }

    public void setUsrModi(String usrModi) {
        this.usrModi = usrModi;
    }

    @Basic
    @Column(name = "fch_modi")
    public Timestamp getFchModi() {
        return fchModi;
    }

    public void setFchModi(Timestamp fchModi) {
        this.fchModi = fchModi;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        BidEstaProc estaProc = (BidEstaProc) o;
        return idEstaProc == estaProc.idEstaProc &&
                idEsta == estaProc.idEsta &&
                idTipo == estaProc.idTipo &&
                Objects.equals(codEstaProc, estaProc.codEstaProc) &&
                Objects.equals(descEstaProc, estaProc.descEstaProc) &&
                Objects.equals(usrCrea, estaProc.usrCrea) &&
                Objects.equals(fchCrea, estaProc.fchCrea) &&
                Objects.equals(usrModi, estaProc.usrModi) &&
                Objects.equals(fchModi, estaProc.fchModi);
    }

    @Override
    public int hashCode() {

        return Objects.hash(idEstaProc, codEstaProc, descEstaProc, idEsta, idTipo, usrCrea, fchCrea, usrModi, fchModi);
    }
}
