package com.teknei.bid.persistence.entities;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Objects;

@Entity
@Table(name = "bid_clie_firm", schema = "bid", catalog = "bid")
@IdClass(BidClieFirmPK.class)
public class BidClieFirm {
    private Long idClie;
    private Long idTipoClie;
    private Long idTipoFirm;
    private Boolean bolObli;
    private Boolean bolAcep;
    private Integer idEsta;
    private Integer idTipo;
    private String usrCrea;
    private Timestamp fchCrea;
    private String usrModi;
    private Timestamp fchModi;
    private String usrOpeCrea;
    private String usrOpeModi;

    @Id
    @Column(name = "id_clie")
    public Long getIdClie() {
        return idClie;
    }

    public void setIdClie(Long idClie) {
        this.idClie = idClie;
    }

    @Id
    @Column(name = "id_tipo_clie")
    public Long getIdTipoClie() {
        return idTipoClie;
    }

    public void setIdTipoClie(Long idTipoClie) {
        this.idTipoClie = idTipoClie;
    }

    @Id
    @Column(name = "id_tipo_firm")
    public Long getIdTipoFirm() {
        return idTipoFirm;
    }

    public void setIdTipoFirm(Long idTipoFirm) {
        this.idTipoFirm = idTipoFirm;
    }

    @Basic
    @Column(name = "bol_obli")
    public Boolean getBolObli() {
        return bolObli;
    }

    public void setBolObli(Boolean bolObli) {
        this.bolObli = bolObli;
    }

    @Basic
    @Column(name = "bol_acep")
    public Boolean getBolAcep() {
        return bolAcep;
    }

    public void setBolAcep(Boolean bolAcep) {
        this.bolAcep = bolAcep;
    }

    @Basic
    @Column(name = "id_esta")
    public Integer getIdEsta() {
        return idEsta;
    }

    public void setIdEsta(Integer idEsta) {
        this.idEsta = idEsta;
    }

    @Basic
    @Column(name = "id_tipo")
    public Integer getIdTipo() {
        return idTipo;
    }

    public void setIdTipo(Integer idTipo) {
        this.idTipo = idTipo;
    }

    @Basic
    @Column(name = "usr_crea")
    public String getUsrCrea() {
        return usrCrea;
    }

    public void setUsrCrea(String usrCrea) {
        this.usrCrea = usrCrea;
    }

    @Basic
    @Column(name = "fch_crea")
    public Timestamp getFchCrea() {
        return fchCrea;
    }

    public void setFchCrea(Timestamp fchCrea) {
        this.fchCrea = fchCrea;
    }

    @Basic
    @Column(name = "usr_modi")
    public String getUsrModi() {
        return usrModi;
    }

    public void setUsrModi(String usrModi) {
        this.usrModi = usrModi;
    }

    @Basic
    @Column(name = "fch_modi")
    public Timestamp getFchModi() {
        return fchModi;
    }

    public void setFchModi(Timestamp fchModi) {
        this.fchModi = fchModi;
    }

    @Basic
    @Column(name = "usr_ope_crea")
    public String getUsrOpeCrea() {
        return usrOpeCrea;
    }

    public void setUsrOpeCrea(String usrOpeCrea) {
        this.usrOpeCrea = usrOpeCrea;
    }

    @Basic
    @Column(name = "usr_ope_modi")
    public String getUsrOpeModi() {
        return usrOpeModi;
    }

    public void setUsrOpeModi(String usrOpeModi) {
        this.usrOpeModi = usrOpeModi;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        BidClieFirm that = (BidClieFirm) o;
        return Objects.equals(idClie, that.idClie) &&
                Objects.equals(idTipoClie, that.idTipoClie) &&
                Objects.equals(idTipoFirm, that.idTipoFirm) &&
                Objects.equals(bolObli, that.bolObli) &&
                Objects.equals(bolAcep, that.bolAcep) &&
                Objects.equals(idEsta, that.idEsta) &&
                Objects.equals(idTipo, that.idTipo) &&
                Objects.equals(usrCrea, that.usrCrea) &&
                Objects.equals(fchCrea, that.fchCrea) &&
                Objects.equals(usrModi, that.usrModi) &&
                Objects.equals(fchModi, that.fchModi) &&
                Objects.equals(usrOpeCrea, that.usrOpeCrea) &&
                Objects.equals(usrOpeModi, that.usrOpeModi);
    }

    @Override
    public int hashCode() {

        return Objects.hash(idClie, idTipoClie, idTipoFirm, bolObli, bolAcep, idEsta, idTipo, usrCrea, fchCrea, usrModi, fchModi, usrOpeCrea, usrOpeModi);
    }
}
