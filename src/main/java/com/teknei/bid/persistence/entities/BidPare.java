package com.teknei.bid.persistence.entities;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "bid_pare", schema = "bid", catalog = "bid")
public class BidPare {
    private long idPare;
    private String descPare;

    @Id
    @Column(name = "id_pare")
    public long getIdPare() {
        return idPare;
    }

    public void setIdPare(long idPare) {
        this.idPare = idPare;
    }

    @Basic
    @Column(name = "desc_pare")
    public String getDescPare() {
        return descPare;
    }

    public void setDescPare(String descPare) {
        this.descPare = descPare;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        BidPare bidPare = (BidPare) o;
        return idPare == bidPare.idPare &&
                Objects.equals(descPare, bidPare.descPare);
    }

    @Override
    public int hashCode() {

        return Objects.hash(idPare, descPare);
    }
}
