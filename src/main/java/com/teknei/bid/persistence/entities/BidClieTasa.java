package com.teknei.bid.persistence.entities;

import javax.persistence.*;
import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.Objects;

@Entity
@Table(name = "bid_clie_tasa", schema = "bid", catalog = "bid")
@IdClass(BidClieTasaPK.class)
public class BidClieTasa {
    private long idClie;
    private long idTipoTasa;
    private BigInteger tasaInt;
    private int idEsta;
    private int idTipo;
    private String usrCrea;
    private Timestamp fchCrea;
    private String usrModi;
    private Timestamp fchModi;
    private BigInteger gatNomi;
    private BigInteger gatReal;
    private Integer plaz;
    private String usrOpeCrea;
    private String usrOpeModi;

    @Id
    @Column(name = "id_clie")
    public long getIdClie() {
        return idClie;
    }

    public void setIdClie(long idClie) {
        this.idClie = idClie;
    }

    @Id
    @Column(name = "id_tipo_tasa")
    public long getIdTipoTasa() {
        return idTipoTasa;
    }

    public void setIdTipoTasa(long idTipoTasa) {
        this.idTipoTasa = idTipoTasa;
    }

    @Basic
    @Column(name = "tasa_int")
    public BigInteger getTasaInt() {
        return tasaInt;
    }

    public void setTasaInt(BigInteger tasaInt) {
        this.tasaInt = tasaInt;
    }

    @Basic
    @Column(name = "id_esta")
    public int getIdEsta() {
        return idEsta;
    }

    public void setIdEsta(int idEsta) {
        this.idEsta = idEsta;
    }

    @Basic
    @Column(name = "id_tipo")
    public int getIdTipo() {
        return idTipo;
    }

    public void setIdTipo(int idTipo) {
        this.idTipo = idTipo;
    }

    @Basic
    @Column(name = "usr_crea")
    public String getUsrCrea() {
        return usrCrea;
    }

    public void setUsrCrea(String usrCrea) {
        this.usrCrea = usrCrea;
    }

    @Basic
    @Column(name = "fch_crea")
    public Timestamp getFchCrea() {
        return fchCrea;
    }

    public void setFchCrea(Timestamp fchCrea) {
        this.fchCrea = fchCrea;
    }

    @Basic
    @Column(name = "usr_modi")
    public String getUsrModi() {
        return usrModi;
    }

    public void setUsrModi(String usrModi) {
        this.usrModi = usrModi;
    }

    @Basic
    @Column(name = "fch_modi")
    public Timestamp getFchModi() {
        return fchModi;
    }

    public void setFchModi(Timestamp fchModi) {
        this.fchModi = fchModi;
    }

    @Basic
    @Column(name = "gat_nomi")
    public BigInteger getGatNomi() {
        return gatNomi;
    }

    public void setGatNomi(BigInteger gatNomi) {
        this.gatNomi = gatNomi;
    }

    @Basic
    @Column(name = "gat_real")
    public BigInteger getGatReal() {
        return gatReal;
    }

    public void setGatReal(BigInteger gatReal) {
        this.gatReal = gatReal;
    }

    @Basic
    @Column(name = "plaz")
    public Integer getPlaz() {
        return plaz;
    }

    public void setPlaz(Integer plaz) {
        this.plaz = plaz;
    }

    @Basic
    @Column(name = "usr_ope_crea")
    public String getUsrOpeCrea() {
        return usrOpeCrea;
    }

    public void setUsrOpeCrea(String usrOpeCrea) {
        this.usrOpeCrea = usrOpeCrea;
    }

    @Basic
    @Column(name = "usr_ope_modi")
    public String getUsrOpeModi() {
        return usrOpeModi;
    }

    public void setUsrOpeModi(String usrOpeModi) {
        this.usrOpeModi = usrOpeModi;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        BidClieTasa that = (BidClieTasa) o;
        return idClie == that.idClie &&
                idTipoTasa == that.idTipoTasa &&
                idEsta == that.idEsta &&
                idTipo == that.idTipo &&
                Objects.equals(tasaInt, that.tasaInt) &&
                Objects.equals(usrCrea, that.usrCrea) &&
                Objects.equals(fchCrea, that.fchCrea) &&
                Objects.equals(usrModi, that.usrModi) &&
                Objects.equals(fchModi, that.fchModi) &&
                Objects.equals(gatNomi, that.gatNomi) &&
                Objects.equals(gatReal, that.gatReal) &&
                Objects.equals(plaz, that.plaz) &&
                Objects.equals(usrOpeCrea, that.usrOpeCrea) &&
                Objects.equals(usrOpeModi, that.usrOpeModi);
    }

    @Override
    public int hashCode() {

        return Objects.hash(idClie, idTipoTasa, tasaInt, idEsta, idTipo, usrCrea, fchCrea, usrModi, fchModi, gatNomi, gatReal, plaz, usrOpeCrea, usrOpeModi);
    }
}
