package com.teknei.bid.persistence.entities;

import javax.persistence.Column;
import javax.persistence.Id;
import java.io.Serializable;
import java.util.Objects;

public class BidEmprCliePK implements Serializable {
    private long idEmpr;
    private long idClie;

    @Column(name = "id_empr")
    @Id
    public long getIdEmpr() {
        return idEmpr;
    }

    public void setIdEmpr(long idEmpr) {
        this.idEmpr = idEmpr;
    }

    @Column(name = "id_clie")
    @Id
    public long getIdClie() {
        return idClie;
    }

    public void setIdClie(long idClie) {
        this.idClie = idClie;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        BidEmprCliePK that = (BidEmprCliePK) o;
        return idEmpr == that.idEmpr &&
                idClie == that.idClie;
    }

    @Override
    public int hashCode() {

        return Objects.hash(idEmpr, idClie);
    }
}
