package com.teknei.bid.persistence.entities;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Objects;

@Entity
@Table(name = "bid_docu_prim", schema = "bid", catalog = "bid")
public class BidDocuPrim {
    private long idDocuPrim;
    private String codDocuPrim;
    private String descDocuPrim;
    private int idEsta;
    private int idTipo;
    private String usrCrea;
    private Timestamp fchCrea;
    private String usrModi;
    private Timestamp fchModi;
    private boolean bolScanAnve;
    private boolean bolScanReve;

    @Id
    @Column(name = "id_docu_prim")
    public long getIdDocuPrim() {
        return idDocuPrim;
    }

    public void setIdDocuPrim(long idDocuPrim) {
        this.idDocuPrim = idDocuPrim;
    }

    @Basic
    @Column(name = "cod_docu_prim")
    public String getCodDocuPrim() {
        return codDocuPrim;
    }

    public void setCodDocuPrim(String codDocuPrim) {
        this.codDocuPrim = codDocuPrim;
    }

    @Basic
    @Column(name = "desc_docu_prim")
    public String getDescDocuPrim() {
        return descDocuPrim;
    }

    public void setDescDocuPrim(String descDocuPrim) {
        this.descDocuPrim = descDocuPrim;
    }

    @Basic
    @Column(name = "id_esta")
    public int getIdEsta() {
        return idEsta;
    }

    public void setIdEsta(int idEsta) {
        this.idEsta = idEsta;
    }

    @Basic
    @Column(name = "id_tipo")
    public int getIdTipo() {
        return idTipo;
    }

    public void setIdTipo(int idTipo) {
        this.idTipo = idTipo;
    }

    @Basic
    @Column(name = "usr_crea")
    public String getUsrCrea() {
        return usrCrea;
    }

    public void setUsrCrea(String usrCrea) {
        this.usrCrea = usrCrea;
    }

    @Basic
    @Column(name = "fch_crea")
    public Timestamp getFchCrea() {
        return fchCrea;
    }

    public void setFchCrea(Timestamp fchCrea) {
        this.fchCrea = fchCrea;
    }

    @Basic
    @Column(name = "usr_modi")
    public String getUsrModi() {
        return usrModi;
    }

    public void setUsrModi(String usrModi) {
        this.usrModi = usrModi;
    }

    @Basic
    @Column(name = "fch_modi")
    public Timestamp getFchModi() {
        return fchModi;
    }

    public void setFchModi(Timestamp fchModi) {
        this.fchModi = fchModi;
    }

    @Basic
    @Column(name = "bol_scan_anve")
    public boolean isBolScanAnve() {
        return bolScanAnve;
    }

    public void setBolScanAnve(boolean bolScanAnve) {
        this.bolScanAnve = bolScanAnve;
    }

    @Basic
    @Column(name = "bol_scan_reve")
    public boolean isBolScanReve() {
        return bolScanReve;
    }

    public void setBolScanReve(boolean bolScanReve) {
        this.bolScanReve = bolScanReve;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        BidDocuPrim that = (BidDocuPrim) o;
        return idDocuPrim == that.idDocuPrim &&
                idEsta == that.idEsta &&
                idTipo == that.idTipo &&
                bolScanAnve == that.bolScanAnve &&
                bolScanReve == that.bolScanReve &&
                Objects.equals(codDocuPrim, that.codDocuPrim) &&
                Objects.equals(descDocuPrim, that.descDocuPrim) &&
                Objects.equals(usrCrea, that.usrCrea) &&
                Objects.equals(fchCrea, that.fchCrea) &&
                Objects.equals(usrModi, that.usrModi) &&
                Objects.equals(fchModi, that.fchModi);
    }

    @Override
    public int hashCode() {

        return Objects.hash(idDocuPrim, codDocuPrim, descDocuPrim, idEsta, idTipo, usrCrea, fchCrea, usrModi, fchModi, bolScanAnve, bolScanReve);
    }
}
