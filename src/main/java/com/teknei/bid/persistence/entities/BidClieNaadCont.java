package com.teknei.bid.persistence.entities;

import org.hibernate.annotations.Immutable;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Immutable
@Table(name = "bid_clie_naad_cont", schema = "bid", catalog = "bid")
public class BidClieNaadCont {
    private Long idClie;
    private String naciAdi;
    private String myKey;

    @Basic
    @Column(name = "my_key")
    @Id
    public String getMyKey() {
        return myKey;
    }

    public void setMyKey(String myKey) {
        this.myKey = myKey;
    }

    @Basic
    @Column(name = "id_clie")
    public Long getIdClie() {
        return idClie;
    }

    public void setIdClie(Long idClie) {
        this.idClie = idClie;
    }

    @Basic
    @Column(name = "naci_adi")
    public String getNaciAdi() {
        return naciAdi;
    }

    public void setNaciAdi(String naciAdi) {
        this.naciAdi = naciAdi;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        BidClieNaadCont that = (BidClieNaadCont) o;
        return Objects.equals(idClie, that.idClie) &&
                Objects.equals(naciAdi, that.naciAdi);
    }

    @Override
    public int hashCode() {

        return Objects.hash(idClie, naciAdi);
    }
}
