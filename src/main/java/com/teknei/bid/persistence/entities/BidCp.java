package com.teknei.bid.persistence.entities;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "bid_cp", schema = "bid", catalog = "bid")
public class BidCp {
    private String idPais;
    private String idEstd;
    private String idMuni;
    private String cp;

    @Basic
    @Column(name = "id_pais")
    public String getIdPais() {
        return idPais;
    }

    public void setIdPais(String idPais) {
        this.idPais = idPais;
    }

    @Basic
    @Column(name = "id_estd")
    public String getIdEstd() {
        return idEstd;
    }

    public void setIdEstd(String idEstd) {
        this.idEstd = idEstd;
    }

    @Basic
    @Column(name = "id_muni")
    public String getIdMuni() {
        return idMuni;
    }

    public void setIdMuni(String idMuni) {
        this.idMuni = idMuni;
    }

    @Id
    @Column(name = "cp")
    public String getCp() {
        return cp;
    }

    public void setCp(String cp) {
        this.cp = cp;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        BidCp bidCp = (BidCp) o;
        return Objects.equals(idPais, bidCp.idPais) &&
                Objects.equals(idEstd, bidCp.idEstd) &&
                Objects.equals(idMuni, bidCp.idMuni) &&
                Objects.equals(cp, bidCp.cp);
    }

    @Override
    public int hashCode() {

        return Objects.hash(idPais, idEstd, idMuni, cp);
    }
}
