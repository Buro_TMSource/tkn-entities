package com.teknei.bid.persistence.entities;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Objects;

@Entity
@Table(name = "bid_empr", schema = "bid", catalog = "bid")
public class BidEmpr {
    private long idEmpr;
    private String nomEmpr;
    private int idEsta;
    private int idTipo;
    private String usrCrea;
    private Timestamp fchCrea;
    private String usrModi;
    private Timestamp fchModi;
    private String usrOpeCrea;
    private String usrOpeModi;

    @Id
    @SequenceGenerator(schema = "bid", name = "BID_EMPR_SEQ", sequenceName = "bid.bid_empr_id_empr_seq", allocationSize = 1, catalog = "bid")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "BID_EMPR_SEQ")
    @Column(name = "id_empr")
    public long getIdEmpr() {
        return idEmpr;
    }

    public void setIdEmpr(long idEmpr) {
        this.idEmpr = idEmpr;
    }

    @Basic
    @Column(name = "nom_empr")
    public String getNomEmpr() {
        return nomEmpr;
    }

    public void setNomEmpr(String nomEmpr) {
        this.nomEmpr = nomEmpr;
    }

    @Basic
    @Column(name = "id_esta")
    public int getIdEsta() {
        return idEsta;
    }

    public void setIdEsta(int idEsta) {
        this.idEsta = idEsta;
    }

    @Basic
    @Column(name = "id_tipo")
    public int getIdTipo() {
        return idTipo;
    }

    public void setIdTipo(int idTipo) {
        this.idTipo = idTipo;
    }

    @Basic
    @Column(name = "usr_crea")
    public String getUsrCrea() {
        return usrCrea;
    }

    public void setUsrCrea(String usrCrea) {
        this.usrCrea = usrCrea;
    }

    @Basic
    @Column(name = "fch_crea")
    public Timestamp getFchCrea() {
        return fchCrea;
    }

    public void setFchCrea(Timestamp fchCrea) {
        this.fchCrea = fchCrea;
    }

    @Basic
    @Column(name = "usr_modi")
    public String getUsrModi() {
        return usrModi;
    }

    public void setUsrModi(String usrModi) {
        this.usrModi = usrModi;
    }

    @Basic
    @Column(name = "fch_modi")
    public Timestamp getFchModi() {
        return fchModi;
    }

    public void setFchModi(Timestamp fchModi) {
        this.fchModi = fchModi;
    }

    @Basic
    @Column(name = "usr_ope_crea")
    public String getUsrOpeCrea() {
        return usrOpeCrea;
    }

    public void setUsrOpeCrea(String usrOpeCrea) {
        this.usrOpeCrea = usrOpeCrea;
    }

    @Basic
    @Column(name = "usr_ope_modi")
    public String getUsrOpeModi() {
        return usrOpeModi;
    }

    public void setUsrOpeModi(String usrOpeModi) {
        this.usrOpeModi = usrOpeModi;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        BidEmpr bidEmpr = (BidEmpr) o;
        return idEmpr == bidEmpr.idEmpr &&
                idEsta == bidEmpr.idEsta &&
                idTipo == bidEmpr.idTipo &&
                Objects.equals(nomEmpr, bidEmpr.nomEmpr) &&
                Objects.equals(usrCrea, bidEmpr.usrCrea) &&
                Objects.equals(fchCrea, bidEmpr.fchCrea) &&
                Objects.equals(usrModi, bidEmpr.usrModi) &&
                Objects.equals(fchModi, bidEmpr.fchModi) &&
                Objects.equals(usrOpeCrea, bidEmpr.usrOpeCrea) &&
                Objects.equals(usrOpeModi, bidEmpr.usrOpeModi);
    }

    @Override
    public int hashCode() {

        return Objects.hash(idEmpr, nomEmpr, idEsta, idTipo, usrCrea, fchCrea, usrModi, fchModi, usrOpeCrea, usrOpeModi);
    }
}
