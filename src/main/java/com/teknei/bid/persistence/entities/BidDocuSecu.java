package com.teknei.bid.persistence.entities;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Objects;

@Entity
@Table(name = "bid_docu_secu", schema = "bid", catalog = "bid")
public class BidDocuSecu {
    private long idDocuSecu;
    private String codDocuSecu;
    private String descDocuSecu;
    private boolean bolScanAnve;
    private boolean bolScanReve;
    private int idEsta;
    private int idTipo;
    private String usrCrea;
    private Timestamp fchCrea;
    private String usrModi;
    private Timestamp fchModi;

    @Id
    @Column(name = "id_docu_secu")
    public long getIdDocuSecu() {
        return idDocuSecu;
    }

    public void setIdDocuSecu(long idDocuSecu) {
        this.idDocuSecu = idDocuSecu;
    }

    @Basic
    @Column(name = "cod_docu_secu")
    public String getCodDocuSecu() {
        return codDocuSecu;
    }

    public void setCodDocuSecu(String codDocuSecu) {
        this.codDocuSecu = codDocuSecu;
    }

    @Basic
    @Column(name = "desc_docu_secu")
    public String getDescDocuSecu() {
        return descDocuSecu;
    }

    public void setDescDocuSecu(String descDocuSecu) {
        this.descDocuSecu = descDocuSecu;
    }

    @Basic
    @Column(name = "bol_scan_anve")
    public boolean isBolScanAnve() {
        return bolScanAnve;
    }

    public void setBolScanAnve(boolean bolScanAnve) {
        this.bolScanAnve = bolScanAnve;
    }

    @Basic
    @Column(name = "bol_scan_reve")
    public boolean isBolScanReve() {
        return bolScanReve;
    }

    public void setBolScanReve(boolean bolScanReve) {
        this.bolScanReve = bolScanReve;
    }

    @Basic
    @Column(name = "id_esta")
    public int getIdEsta() {
        return idEsta;
    }

    public void setIdEsta(int idEsta) {
        this.idEsta = idEsta;
    }

    @Basic
    @Column(name = "id_tipo")
    public int getIdTipo() {
        return idTipo;
    }

    public void setIdTipo(int idTipo) {
        this.idTipo = idTipo;
    }

    @Basic
    @Column(name = "usr_crea")
    public String getUsrCrea() {
        return usrCrea;
    }

    public void setUsrCrea(String usrCrea) {
        this.usrCrea = usrCrea;
    }

    @Basic
    @Column(name = "fch_crea")
    public Timestamp getFchCrea() {
        return fchCrea;
    }

    public void setFchCrea(Timestamp fchCrea) {
        this.fchCrea = fchCrea;
    }

    @Basic
    @Column(name = "usr_modi")
    public String getUsrModi() {
        return usrModi;
    }

    public void setUsrModi(String usrModi) {
        this.usrModi = usrModi;
    }

    @Basic
    @Column(name = "fch_modi")
    public Timestamp getFchModi() {
        return fchModi;
    }

    public void setFchModi(Timestamp fchModi) {
        this.fchModi = fchModi;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        BidDocuSecu that = (BidDocuSecu) o;
        return idDocuSecu == that.idDocuSecu &&
                bolScanAnve == that.bolScanAnve &&
                bolScanReve == that.bolScanReve &&
                idEsta == that.idEsta &&
                idTipo == that.idTipo &&
                Objects.equals(codDocuSecu, that.codDocuSecu) &&
                Objects.equals(descDocuSecu, that.descDocuSecu) &&
                Objects.equals(usrCrea, that.usrCrea) &&
                Objects.equals(fchCrea, that.fchCrea) &&
                Objects.equals(usrModi, that.usrModi) &&
                Objects.equals(fchModi, that.fchModi);
    }

    @Override
    public int hashCode() {

        return Objects.hash(idDocuSecu, codDocuSecu, descDocuSecu, bolScanAnve, bolScanReve, idEsta, idTipo, usrCrea, fchCrea, usrModi, fchModi);
    }
}
