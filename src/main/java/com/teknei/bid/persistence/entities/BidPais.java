package com.teknei.bid.persistence.entities;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "bid_pais", schema = "bid", catalog = "bid")
public class BidPais {
    private String idPais;
    private String nomPais;

    @Id
    @Column(name = "id_pais")
    public String getIdPais() {
        return idPais;
    }

    public void setIdPais(String idPais) {
        this.idPais = idPais;
    }

    @Basic
    @Column(name = "nom_pais")
    public String getNomPais() {
        return nomPais;
    }

    public void setNomPais(String nomPais) {
        this.nomPais = nomPais;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        BidPais bidPais = (BidPais) o;
        return Objects.equals(idPais, bidPais.idPais) &&
                Objects.equals(nomPais, bidPais.nomPais);
    }

    @Override
    public int hashCode() {

        return Objects.hash(idPais, nomPais);
    }
}
