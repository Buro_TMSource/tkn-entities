package com.teknei.bid.persistence.entities;

import javax.persistence.Column;
import javax.persistence.Id;
import java.io.Serializable;
import java.util.Objects;

public class BidClieRegProcPK implements Serializable {
    private long idClie;
    private String curp;

    @Column(name = "id_clie")
    @Id
    public long getIdClie() {
        return idClie;
    }

    public void setIdClie(long idClie) {
        this.idClie = idClie;
    }

    @Column(name = "curp")
    @Id
    public String getCurp() {
        return curp;
    }

    public void setCurp(String curp) {
        this.curp = curp;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        BidClieRegProcPK that = (BidClieRegProcPK) o;
        return idClie == that.idClie &&
                Objects.equals(curp, that.curp);
    }

    @Override
    public int hashCode() {

        return Objects.hash(idClie, curp);
    }
}
