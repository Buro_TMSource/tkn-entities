package com.teknei.bid.persistence.entities;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;

public class BidClieIfeIneBitaPK implements Serializable {
    private long idClie;
    private long idIfe;
    private long idIfeIneBita;

    @Column(name = "id_clie")
    @Id
    public long getIdClie() {
        return idClie;
    }

    public void setIdClie(long idClie) {
        this.idClie = idClie;
    }

    @Column(name = "id_ife")
    @Id
    public long getIdIfe() {
        return idIfe;
    }

    public void setIdIfe(long idIfe) {
        this.idIfe = idIfe;
    }

    @Column(name = "id_ife_ine_bita")
    @SequenceGenerator(sequenceName = "bid.bid_clie_ife_ine_bita_id_ife_ine_bita_seq", name = "ine_bita_seq_pk_gen", allocationSize = 1)
    @GeneratedValue(generator = "ine_bita_seq_pk_gen", strategy = GenerationType.SEQUENCE)
    @Id
    public long getIdIfeIneBita() {
        return idIfeIneBita;
    }

    public void setIdIfeIneBita(long idIfeIneBita) {
        this.idIfeIneBita = idIfeIneBita;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        BidClieIfeIneBitaPK that = (BidClieIfeIneBitaPK) o;
        return idClie == that.idClie &&
                idIfe == that.idIfe &&
                idIfeIneBita == that.idIfeIneBita;
    }

    @Override
    public int hashCode() {

        return Objects.hash(idClie, idIfe, idIfeIneBita);
    }
}
