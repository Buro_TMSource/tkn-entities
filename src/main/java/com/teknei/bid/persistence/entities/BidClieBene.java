package com.teknei.bid.persistence.entities;

import javax.persistence.*;
import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.Objects;

@Entity
@Table(name = "bid_clie_bene", schema = "bid", catalog = "bid")
@IdClass(BidClieBenePK.class)
public class BidClieBene {
    private long idClie;
    private long idPare;
    private long idClieBene;
    private String nombBene;
    private String fchNac;
    private String domiBene;
    private BigInteger porcPart;
    private int idEsta;
    private int idTipo;
    private String usrCrea;
    private Timestamp fchCrea;
    private String usrModi;
    private Timestamp fchModi;
    private String usrOpeCrea;
    private String usrOpeModi;

    @Id
    @Column(name = "id_clie")
    public long getIdClie() {
        return idClie;
    }

    public void setIdClie(long idClie) {
        this.idClie = idClie;
    }

    @Id
    @Column(name = "id_pare")
    public long getIdPare() {
        return idPare;
    }

    public void setIdPare(long idPare) {
        this.idPare = idPare;
    }

    @Id
    @Column(name = "id_clie_bene")
    public long getIdClieBene() {
        return idClieBene;
    }

    public void setIdClieBene(long idClieBene) {
        this.idClieBene = idClieBene;
    }

    @Basic
    @Column(name = "nomb_bene")
    public String getNombBene() {
        return nombBene;
    }

    public void setNombBene(String nombBene) {
        this.nombBene = nombBene;
    }

    @Basic
    @Column(name = "fch_nac")
    public String getFchNac() {
        return fchNac;
    }

    public void setFchNac(String fchNac) {
        this.fchNac = fchNac;
    }

    @Basic
    @Column(name = "domi_bene")
    public String getDomiBene() {
        return domiBene;
    }

    public void setDomiBene(String domiBene) {
        this.domiBene = domiBene;
    }

    @Basic
    @Column(name = "porc_part")
    public BigInteger getPorcPart() {
        return porcPart;
    }

    public void setPorcPart(BigInteger porcPart) {
        this.porcPart = porcPart;
    }

    @Basic
    @Column(name = "id_esta")
    public int getIdEsta() {
        return idEsta;
    }

    public void setIdEsta(int idEsta) {
        this.idEsta = idEsta;
    }

    @Basic
    @Column(name = "id_tipo")
    public int getIdTipo() {
        return idTipo;
    }

    public void setIdTipo(int idTipo) {
        this.idTipo = idTipo;
    }

    @Basic
    @Column(name = "usr_crea")
    public String getUsrCrea() {
        return usrCrea;
    }

    public void setUsrCrea(String usrCrea) {
        this.usrCrea = usrCrea;
    }

    @Basic
    @Column(name = "fch_crea")
    public Timestamp getFchCrea() {
        return fchCrea;
    }

    public void setFchCrea(Timestamp fchCrea) {
        this.fchCrea = fchCrea;
    }

    @Basic
    @Column(name = "usr_modi")
    public String getUsrModi() {
        return usrModi;
    }

    public void setUsrModi(String usrModi) {
        this.usrModi = usrModi;
    }

    @Basic
    @Column(name = "fch_modi")
    public Timestamp getFchModi() {
        return fchModi;
    }

    public void setFchModi(Timestamp fchModi) {
        this.fchModi = fchModi;
    }

    @Basic
    @Column(name = "usr_ope_crea")
    public String getUsrOpeCrea() {
        return usrOpeCrea;
    }

    public void setUsrOpeCrea(String usrOpeCrea) {
        this.usrOpeCrea = usrOpeCrea;
    }

    @Basic
    @Column(name = "usr_ope_modi")
    public String getUsrOpeModi() {
        return usrOpeModi;
    }

    public void setUsrOpeModi(String usrOpeModi) {
        this.usrOpeModi = usrOpeModi;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        BidClieBene that = (BidClieBene) o;
        return idClie == that.idClie &&
                idPare == that.idPare &&
                idClieBene == that.idClieBene &&
                idEsta == that.idEsta &&
                idTipo == that.idTipo &&
                Objects.equals(nombBene, that.nombBene) &&
                Objects.equals(fchNac, that.fchNac) &&
                Objects.equals(domiBene, that.domiBene) &&
                Objects.equals(porcPart, that.porcPart) &&
                Objects.equals(usrCrea, that.usrCrea) &&
                Objects.equals(fchCrea, that.fchCrea) &&
                Objects.equals(usrModi, that.usrModi) &&
                Objects.equals(fchModi, that.fchModi) &&
                Objects.equals(usrOpeCrea, that.usrOpeCrea) &&
                Objects.equals(usrOpeModi, that.usrOpeModi);
    }

    @Override
    public int hashCode() {

        return Objects.hash(idClie, idPare, idClieBene, nombBene, fchNac, domiBene, porcPart, idEsta, idTipo, usrCrea, fchCrea, usrModi, fchModi, usrOpeCrea, usrOpeModi);
    }
}
