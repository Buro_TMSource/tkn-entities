package com.teknei.bid.persistence.entities;

import javax.persistence.Column;
import javax.persistence.Id;
import java.io.Serializable;
import java.util.Objects;

public class BidClieDireNestPK implements Serializable {
    private long idClie;
    private long idDire;

    @Column(name = "id_clie")
    @Id
    public long getIdClie() {
        return idClie;
    }

    public void setIdClie(long idClie) {
        this.idClie = idClie;
    }

    @Column(name = "id_dire")
    @Id
    public long getIdDire() {
        return idDire;
    }

    public void setIdDire(long idDire) {
        this.idDire = idDire;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        BidClieDireNestPK that = (BidClieDireNestPK) o;
        return idClie == that.idClie &&
                idDire == that.idDire;
    }

    @Override
    public int hashCode() {

        return Objects.hash(idClie, idDire);
    }
}
