package com.teknei.bid.persistence.repository;

import com.teknei.bid.persistence.entities.BidTipoFirm;
import org.springframework.data.jpa.repository.JpaRepository;

public interface BidTipoFirmRepository extends JpaRepository<BidTipoFirm, Long> {

    BidTipoFirm findByCodTipoFirmAndIdEsta(String codTipoFirm, Integer idEsta);

}
