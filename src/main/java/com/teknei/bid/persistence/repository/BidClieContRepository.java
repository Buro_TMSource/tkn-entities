package com.teknei.bid.persistence.repository;

import com.teknei.bid.persistence.entities.BidClieCont;
import org.springframework.data.jpa.repository.JpaRepository;

public interface BidClieContRepository extends JpaRepository<BidClieCont, Long> {

    BidClieCont findByIdClie(Long idClie);

}