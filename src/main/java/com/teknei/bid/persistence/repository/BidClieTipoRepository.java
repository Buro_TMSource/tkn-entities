package com.teknei.bid.persistence.repository;

import com.teknei.bid.persistence.entities.BidClieTipo;
import com.teknei.bid.persistence.entities.BidClieTipoPK;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

public interface BidClieTipoRepository extends JpaRepository<BidClieTipo, BidClieTipoPK> {
	List<BidClieTipo>  findAllByIdClie(Long idClient);
}
