package com.teknei.bid.persistence.repository;

import com.teknei.bid.persistence.entities.BidClieMail;
import com.teknei.bid.persistence.entities.BidClieMailPK;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by Amaro on 28/07/2017.
 */
public interface BidClieMailRepository extends JpaRepository<BidClieMail, BidClieMailPK> {

    BidClieMail findTopByIdClie(Long idClie);

}