package com.teknei.bid.persistence.repository;

import com.teknei.bid.persistence.entities.BidClieBene;
import com.teknei.bid.persistence.entities.BidClieBenePK;
import org.springframework.data.jpa.repository.JpaRepository;

public interface BidClieBeneRepository extends JpaRepository<BidClieBene, BidClieBenePK> {
}
