package com.teknei.bid.persistence.repository;

import com.teknei.bid.persistence.entities.BidClieIfeIne;
import com.teknei.bid.persistence.entities.BidClieIfeInePK;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface BidIfeRepository extends JpaRepository<BidClieIfeIne, BidClieIfeInePK> {

    @Query(value = "select nextval('bid.bid_ceco_ban_seq')", nativeQuery = true)
    Long getCecobanId();

    List<BidClieIfeIne> findByClavElecAndOcrAndMrz(String clavElec, String ocr, String mrz);

    List<BidClieIfeIne> findByMrzAndOcr(String mrz, String ocr);

    List<BidClieIfeIne> findByMrz(String mrz);

    List<BidClieIfeIne> findByOcr(String ocr);

    List<BidClieIfeIne> findByOcrAndClavElec(String ocr, String clavElec);

    List<BidClieIfeIne> findByMrzAndClavElec(String mrz, String clavElec);

    List<BidClieIfeIne> findByClavElec(String clavElec);

}