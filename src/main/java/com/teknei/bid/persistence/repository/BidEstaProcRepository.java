package com.teknei.bid.persistence.repository;

import com.teknei.bid.persistence.entities.BidEstaProc;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface BidEstaProcRepository extends JpaRepository<BidEstaProc, Long> {

    List<BidEstaProc> findAllByIdEstaOrderByIdEstaProc(Integer idEsta);

    BidEstaProc findTopByCodEstaProcAndIdEsta(String codEstaProc, Integer idEsta);

}