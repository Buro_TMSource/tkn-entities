package com.teknei.bid.persistence.repository;

import com.teknei.bid.persistence.entities.BidClieCert;
import com.teknei.bid.persistence.entities.BidClieCertPK;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface BidClieCertRepository extends JpaRepository<BidClieCert, BidClieCertPK> {

    List<BidClieCert> findAllByIdClieAndIdEsta(Long idClie, Integer idEsta);

}
