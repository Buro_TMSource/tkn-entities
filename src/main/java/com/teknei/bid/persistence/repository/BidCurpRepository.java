package com.teknei.bid.persistence.repository;

import com.teknei.bid.persistence.entities.BidClieCurp;
import com.teknei.bid.persistence.entities.BidClieCurpPK;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface BidCurpRepository extends JpaRepository<BidClieCurp, BidClieCurpPK> {

    BidClieCurp findFirstByCurp(String curp);
    
    BidClieCurp findTopByCurpAndIdTipo(String curp,Integer idTipo);

    BidClieCurp findTopByIdClie(Long idClie);

    @Modifying
    @Query("UPDATE BidClieCurp b SET b.curp = :curp WHERE b.idClie = :idClie")
    void updateCurp(@Param("curp") String curp, @Param("idClie") Long idClie);
}
