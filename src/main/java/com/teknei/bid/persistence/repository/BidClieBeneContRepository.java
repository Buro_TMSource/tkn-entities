package com.teknei.bid.persistence.repository;

import com.teknei.bid.persistence.entities.BidClieBeneCont;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface BidClieBeneContRepository extends JpaRepository<BidClieBeneCont, Long>{

    List<BidClieBeneCont> findAllByIdClie(Long idClie);

}