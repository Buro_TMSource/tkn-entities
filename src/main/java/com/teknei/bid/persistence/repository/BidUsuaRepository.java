package com.teknei.bid.persistence.repository;

import com.teknei.bid.persistence.entities.BidUsua;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface BidUsuaRepository extends JpaRepository<BidUsua, Long> {

    List<BidUsua> findByIdEsta(Integer idEsta);

}