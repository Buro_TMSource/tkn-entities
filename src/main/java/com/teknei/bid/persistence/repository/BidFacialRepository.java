package com.teknei.bid.persistence.repository;

import com.teknei.bid.persistence.entities.BidClieFaci;
import com.teknei.bid.persistence.entities.BidClieFaciPK;
import org.springframework.data.jpa.repository.JpaRepository;

public interface BidFacialRepository extends JpaRepository<BidClieFaci, BidClieFaciPK> {
}
