package com.teknei.bid.persistence.repository;

import com.teknei.bid.persistence.entities.BidClie;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

/**
 * Created by Amaro on 28/07/2017.
 */
public interface BidClieRepository extends JpaRepository<BidClie, Long> {
	//idTipo
	@Modifying
	@Query("UPDATE BidClie b SET b.idTipo = :idTipo WHERE b.idClie = :idClie")
	void updateIdTipo(@Param("idClie") Long idClie, @Param("idTipo") Integer idTipo);
}
