package com.teknei.bid.persistence.repository;

import com.teknei.bid.persistence.entities.BidClieMail;
import com.teknei.bid.persistence.entities.BidClieMailPK;
import org.springframework.data.jpa.repository.JpaRepository;

public interface BidMailRepository extends JpaRepository<BidClieMail, BidClieMailPK> {

    BidClieMail findTopByIdClie(Long idClie);

    BidClieMail findTopByEmai(String emai);

    BidClieMail findByEmai(String emai);

}