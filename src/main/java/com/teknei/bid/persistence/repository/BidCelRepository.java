package com.teknei.bid.persistence.repository;

import com.teknei.bid.persistence.entities.BidCel;
import com.teknei.bid.persistence.entities.BidCelPK;
import com.teknei.bid.persistence.entities.BidTel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface BidCelRepository extends JpaRepository<BidCel, BidCelPK> {

    BidCel findTopByNumCel(String numCel);

    BidCel findTopByIdClie(Long idClie);

    List<BidCel> findAllByIdClie(Long idClie);

    List<BidCel> findAllByNumCel(String numCel);

    @Modifying
    @Query("UPDATE BidCel b SET b.numCel = :numCel WHERE b.idClie = :idClie")
    void updateNumCel(@Param("idClie") Long idClie, @Param("numCel") String tel);

    List<BidCel> findAllByIdClieAndIdEsta(Long id, Integer idEsta);
}
