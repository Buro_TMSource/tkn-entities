package com.teknei.bid.persistence.repository;

import com.teknei.bid.persistence.entities.BidClieRfis;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface BidClieRfisRepository extends JpaRepository<BidClieRfis,Long > {

    List<BidClieRfis> findAllByIdClieAndIdEsta(Long idClie, Integer idEsta);

}