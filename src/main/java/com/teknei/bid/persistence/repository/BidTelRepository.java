package com.teknei.bid.persistence.repository;

import com.teknei.bid.persistence.entities.BidTel;
import com.teknei.bid.persistence.entities.BidTelPK;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface BidTelRepository extends JpaRepository<BidTel, BidTelPK> {

    BidTel findTopByTele(String tele);

    BidTel findTopByIdClie(Long idClie);

    @Modifying
    @Query("UPDATE BidTel b SET b.tele = :tele WHERE b.idClie = :idClie")
    void updateTel(@Param("idClie") Long idClie, @Param("tele") String tel);

    List<BidTel> findAllByIdClieAndIdEsta(Long id, Integer idEsta);

}