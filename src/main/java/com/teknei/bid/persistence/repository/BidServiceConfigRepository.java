package com.teknei.bid.persistence.repository;
import org.springframework.data.jpa.repository.JpaRepository;
import com.teknei.bid.persistence.entities.BidServiceConfig;
public interface BidServiceConfigRepository 
	extends JpaRepository<BidServiceConfig, String> 
{
	BidServiceConfig findByStatus(String active);
}
 
